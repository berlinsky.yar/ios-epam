import UIKit

let bottomLimit = 0
let upperLimit = 20

func listGen(size: Int) -> LinkedList<Int> {
    let linkedList = LinkedList<Int>()
    for _ in 0..<size {
        linkedList.append(value: Int.random(in: bottomLimit...upperLimit))
    }
    return linkedList
}

class Node<T: Equatable> {
    var value: T
    var child: Node<T>?
    
    init(value: T) {
        self.value = value
    }
    
    func append(newvalue: T) {
        if let childNode = child {
            childNode.append(newvalue: newvalue)
        } else {
            child = Node<T>(value: newvalue)
        }
    }
}


class LinkedList<T: Equatable> {
    var head: Node<T>?
    
    func append(value: T){
        if head == nil {
            head = Node<T>(value: value)
        } else {
            head!.append(newvalue: value)
        }
    }
    
    func remove(value: T){
        guard head == nil else {
            if head!.value == value {
                head = head!.child
            } else {
                var current = head
                var prev = head
                while current!.child != nil && current!.value != value {
                    prev = current
                    current = current!.child
                }
                if let deleted = current {
                    if deleted.value == value {
                        if let childPtr = deleted.child {
                            prev?.child = childPtr
                            deleted.child = nil
                        } else {
                            prev?.child = nil
                        }
                    }
                }
            }
            return
        }
    }
    var size: Int {
        var current = head
        var counter = 0
        while current != nil {
            current = current?.child
            counter += 1
        }
        return counter
    }
    func output() {
        var str = ""
        var current = head
        while current != nil {
            str += current?.child == nil ? "\(current!.value) => nil" : "\(current!.value) => "
            current = current!.child
        }
        print(str)
    }
}


var linkedList = listGen(size: 20)
linkedList.output()


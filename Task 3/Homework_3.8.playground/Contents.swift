// Binary Tree node. Comparable generic type was declared to provide binary-search-function and avoid possible errors(if Associated type wouldn't comform to Comparable-protocol)
let upperLimit = 0
let bottomLimit = 20
func genValues() -> [Int] {
    var array: [Int] = []
    for _ in upperLimit...bottomLimit {
        array.append(Int.random(in: 0...100))
    }
    return array
}

class Node<T: Comparable> {
    var value: T
    var leftPtr: Node?
    var rightPtr: Node?
    var parent: Node?
    
    init(value: T) {
        self.value = value
    }
    
    func append(value: T) {
        append(value: value, parent: self)
    }

    func search(value: T) -> Bool {
        if value == self.value {
            return true
        }
        if value < self.value {
            if let leftPtr = leftPtr {
                return leftPtr.search(value: value)
            }
        } else {
            if let rightPtr = rightPtr {
                return rightPtr.search(value: value)
            }
        }
        return false
    }
    
    func postOutput(node: Node?, indent: Int = 0) {
        if node != nil {
            if node?.rightPtr != nil {
                postOutput(node: node?.rightPtr, indent: indent + 1)
            }
            if indent != 0 {
                print(getIndent(indent: indent), terminator: " ")
            }
            if node?.rightPtr != nil {
                print(" ➚\n\(getIndent(indent: indent))", terminator: " ")
            }
            print(node?.value ?? "nil")
            if node?.leftPtr != nil {
                print("\(getIndent(indent: indent)) ➘")
                postOutput(node: node?.leftPtr, indent: indent + 1)
            }
        }
    }
}

extension Node {
    private func getIndent(indent: Int) -> String {
        var str = ""
        for _ in 0..<indent {
            str += " "
        }
        return str
    }
    private func append(value: T, parent: Node) {
        if value < self.value {
            if let leftPtr = leftPtr {
                leftPtr.append(value: value, parent: self)
            } else {
                leftPtr = Node(value: value)
                leftPtr?.parent = self
            }
        } else if value > self.value {
            if let rightPtr = rightPtr {
                rightPtr.append(value: value, parent: self)
            } else {
                rightPtr = Node(value: value)
                rightPtr?.parent = self
            }
        }
    }
}

let randArray = genValues()
let tree = Node(value: randArray[0])
for i in 1..<randArray.count {
    tree.append(value: randArray[i])
}

tree.postOutput(node: tree)



func +<T: Numeric>(lhs: T, rhs: T) -> T {
    return lhs + rhs
}

enum MyOptional<T: Equatable>: Equatable {
    static func == (lhs: MyOptional<T>, rhs: MyOptional<T>) -> Bool {
        return lhs.forceUnwrap() == rhs.forceUnwrap()
    }

    case none
    case value(T)

    func forceUnwrap() -> T? {
        switch self {
        case .value(let x):
            return x
        case .none:
            return nil
        }
    }
}

func sum<T: Numeric>(_ value1: MyOptional<T>, _ value2: MyOptional<T>) -> MyOptional<T> {
    switch (value1, value2) {
    case (.none, .none), (value1, .none), (.none, value2):
        return .none
    default:
        return .value(value1.forceUnwrap()! + value2.forceUnwrap()!)
    }
}


var val1: MyOptional<Int> = .none
var val2: MyOptional<Int> = .none
print("Sum: \(sum(val1, val2))")

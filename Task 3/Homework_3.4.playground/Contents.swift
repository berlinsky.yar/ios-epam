import UIKit

indirect enum Solution {
    enum type {
        case double(Double)
        case complex(ComplexNumber)
    }
    case tuple(type, type)
    case error(Double)
    
    func describe() {
        switch self {
        case .error(let discriminant):
            print("There's no solution on real axis. Discriminant = \(discriminant)")
        case .tuple(.double(let x1), .double(let x2)):
            if x1 == x2 { print("X = \(x1)") }
            else { print("X₁ = \(x1); X₂ = \(x2)") }
        case .tuple(.complex(let x1), .complex(let x2)):
            print("X₁ = \(x1.show()); X₂ = \(x2.show())")
        default:
            break
        }
    }
}

class ComplexNumber {
    
    var real: Double
    var imag: Double
    init(_ real: Double, _ imag: Double) {
        self.real = real
        self.imag = imag
    }
    func show() -> String {
        switch (real, imag) {
        case (0, 0), (_, 0):
            return "\(real)"
        case (0, _):
            return "\(imag)i"
        case (_, 1), (_, -1):
            let indicator = (imag > 0) ? "+" : "-"
            return "\(real) \(indicator) i"
        default:
            let indicator = (imag > 0) ? "+" : "-"
            return "\(real) \(indicator) \(abs(imag))i"
        }
    }
    static func == (lhs: ComplexNumber, rhs: ComplexNumber) -> Bool {
        return lhs.real == rhs.real && lhs.imag == rhs.imag
    }
    static prefix func ~(_ num: ComplexNumber) -> ComplexNumber {
        return ComplexNumber(num.real, -num.imag)
    }
    static func -(_ lhs: ComplexNumber, _ rhs: ComplexNumber) -> ComplexNumber {
        return ComplexNumber(lhs.real - rhs.real, lhs.imag - rhs.imag)
    }
    static func +(_ lhs: ComplexNumber, _ rhs: ComplexNumber) -> ComplexNumber {
        return ComplexNumber(lhs.real + rhs.real, lhs.imag + rhs.imag)
    }
    static func *(_ lhs: ComplexNumber, _ rhs: ComplexNumber) -> ComplexNumber {
        return ComplexNumber(lhs.real*rhs.real - lhs.imag*rhs.imag, lhs.real*rhs.imag + rhs.real*lhs.imag)
    }
    static func /(_ lhs: ComplexNumber, _ rhs: ComplexNumber) -> ComplexNumber? {
        guard rhs.real == 0 && rhs.imag == 0 else {
            return ComplexNumber((lhs*(~rhs)).real/(rhs*(~rhs)).real, (lhs*(~rhs)).imag/(rhs*(~rhs)).real)
        }
        return nil
    }
}


func solveSquareEquation(a: Double, b: Double, c: Double, include complex: Bool = false) -> Solution {
    guard a != 0 else {
        return .tuple(.double(-c/b), .double(-c/b))
    }
    let discriminant: Double = pow(b, 2) - 4*a*c
    switch discriminant {
    case _ where discriminant < 0 && !complex:
        return .error(discriminant)
    case _ where discriminant < 0:
        let root1 = ComplexNumber(-b/(2*a), sqrt(abs(discriminant))/(2*a))
        let root2 = ComplexNumber(-b/(2*a), -sqrt(abs(discriminant))/(2*a))
        return .tuple(.complex(root1), .complex(root2))
    default:
        let root1 = (-b + sqrt(discriminant))/(2*a)
        let root2 = (-b - sqrt(discriminant))/(2*a)
        return .tuple(.double(root1), .double(root2))
    }
}

let a: Double = 1
let b: Double = -6
let c: Double = 13
print("\(a)x² + (\(b))x + (\(c)) = 0")
solveSquareEquation(a: a, b: b, c: c, include: true).describe()

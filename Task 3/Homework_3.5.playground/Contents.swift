import UIKit

class FilterIntArray {
    func getEven(array: [Int]) -> [Int] {
        return array.filter({$0 % 2 == 0})
    }
    func getSum(array: [Int]) -> Int {
        return array.reduce(0) { (sum, element) in
            sum + element
        }
    }
    // an indicator PREVENTLOSSES means that if there'll be any unconvertable(to Int) value inside the String Array, the whole array cannot be converted to [Int]
    func convertFromString(line: [String], preventLosses: Bool = false) -> [Int]? {
        var result: [Int] = []
        for element in line {
            if preventLosses && Int(element) == nil { return nil }
            if Int(element) != nil {
                result.append(Int(element)!)
            }
        }
        return result
    }
    func output(array: [Int]?) {
        print("Array: ", terminator: "")
        guard array != nil else {
            print("nil")
            return
        }
        print("[", terminator: "")
        for i in 0..<array!.count {
            let terminator = (i == array!.count - 1) ? "" : ", "
            print(array![i], terminator: terminator)
        }
        print("]")
    }
}

let filtering = FilterIntArray()
let ex1 = [1, 2, 12, 5, 7, 88]
let ex2 = ["1", "2", "12", "bla", "5", "7", "88"]
filtering.output(array: filtering.getEven(array: ex1))
filtering.output(array: filtering.convertFromString(line: ex2, preventLosses: true))

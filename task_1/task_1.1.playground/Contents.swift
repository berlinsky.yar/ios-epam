import UIKit

//Constants
let maxNumberOfVariables = 10
let bottomLimit = 0
let upperLimit = 100
let numberOfVariables = Int.random(in: 2...maxNumberOfVariables)

//Functional Part
func generateVariablesArray(size: Int, range: ClosedRange<Int>) -> [Int] {
    var variables: [Int] = []
    for _ in 0..<size {
        variables.append(Int.random(in: range))
    }
    return variables
}
func calcGeometricMean(variables: [Int]) -> Double {
    var result: Double = 1
    for i in variables {
        result *= pow(Double(i), 1.0/3.0)
    }
    return result
}
func calcAverage(variables: [Int]) -> Double {
    var result: Double = 0
    for i in variables {
        result += Double(i)/Double(variables.count)
    }
    return result
}


//Printing input variables
let variables = generateVariablesArray(size: numberOfVariables, range: bottomLimit...upperLimit)
print("Variables: [", terminator: "")
for i in 0..<variables.count {
    let printTerminator = i == numberOfVariables-1 ? "]" : ", "
    print("\(variables[i])", terminator: printTerminator)
}

//Calculating and printing results
let average = calcAverage(variables: variables)
let geometricMean = calcGeometricMean(variables: variables)

print(
"""
\nTheir average is: \(String(format: "%.3f", average))
\nTheir geometric mean is: \(String(format: "%.3f", geometricMean))
"""
)

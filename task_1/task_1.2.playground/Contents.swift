import UIKit

let bottomLimit = -10.0
let upperLimit = 10.0

let a: Double = Double(String(format: "%.2f", Double.random(in: bottomLimit...upperLimit)))!
let b: Double = Double(String(format: "%.2f", Double.random(in: bottomLimit...upperLimit)))!
let c: Double = Double(String(format: "%.2f", Double.random(in: bottomLimit...upperLimit)))!

func quadraticEquationSolver(a: Double, b: Double, c: Double) -> (String, (Double?, Double?)) {
    var root1, root2: Double?
    var answer = "\(a)*x^2 + (\(b))*x + (\(c)) = 0"
    if a != 0.0 {
        let discriminant = pow(b, 2.0) - 4*a*c
        if discriminant < 0.0 {
            answer += ", no real roots(0>D = \(discriminant))"
        } else {
            root1 = (-b-sqrt(discriminant))/(2*a)
            root2 = (-b+sqrt(discriminant))/(2*a)
            answer += root1 != root2 ? String(format: ", root1 = %.2f, root2 = %.2f", root1!, root2!) : String(format: ", root = %.2f", root1!)
        }
    } else {
        root1 = -c/b
        root2 = root1
        answer = String(format: "(\(b))*x + (\(c)) = 0, root = %.2f", root1!)
    }
    return (answer, (root1, root2))
}

print("A: \(a), B: \(b), C: \(c)")

let answer = quadraticEquationSolver(a: a, b: b, c: c)

print(answer.0)

import UIKit

// types of numbers
enum Number {
    case even
    case odd
}

// constants and random size of array
let bottomSizeLimit = 0; let upperSizeLimit = 10 //for array size range
let bottomElementLimit = 0; let upperElementLimit = 1000 // for element size range
let arraySize = Int.random(in: bottomSizeLimit...upperSizeLimit)

// initializing the array
var array: [Int] = []
for _ in 0..<arraySize {
    array.append(Int.random(in: bottomElementLimit...upperElementLimit))
}

// printing array using For-In
func printArrayUsingFor(array: [Int]) {
    print("Array(with For-In): [", terminator: " ")
    for i in 0..<array.count {
        let printTerminator = i == array.count-1 ? " " : ", "
        print("\(array[i])", terminator: printTerminator)
    }
    print("]\nSize: \(array.count)\n")
}

// printing array using While
func printArrayUsingWhile(array: [Int]) {
    print("\nArray(with While): [", terminator: " ")
    var counter = 0
    while (counter < array.count) {
        let printTerminator = counter == array.count-1 ? " " : ", "
        if array.count != 0 {
            print("\(array[counter])", terminator: printTerminator)
        }
        counter += 1
    }
    print("]\nSize: \(array.count)\n")
}

// printing an input array using Repeat-While
func printArrayUsingRepeat(array: [Int]) {
    print("\nArray(with Repeat-While): [", terminator: " ")
    var counter = 0
    repeat {
        let printTerminator = counter == array.count-1 ? " " : ", "
        if array.count != 0 {
            print("\(array[counter])", terminator: printTerminator)
        }
        counter += 1
    } while (counter < array.count)
    print("]\nSize: \(array.count)\n")
}

// func for finding even or odd numbers using For-In
func findNumbersUsingFor(array: [Int], type: Number) -> [Int] {
    var numbers: [Int] = []
    switch type {
    case .even:
        for i in 0..<array.count {
            if (array[i] % 2 == 0) {
                numbers.append(array[i])
            }
        }
    case .odd:
        for i in 0..<array.count {
            if (array[i] % 2 != 0) {
                numbers.append(array[i])
            }
        }
    }
    return numbers
}

// func for finding even or odd numbers using While
func findNumbersUsingWhile(array: [Int], type: Number) -> [Int] {
    var numbers: [Int] = []
    var counter = 0
    switch type {
    case .even:
        while counter < array.count {
            if (array[counter] % 2 == 0) {
                numbers.append(array[counter])
            }
            counter += 1
        }
    case .odd:
        while counter < array.count {
            if (array[counter] % 2 != 0) {
                numbers.append(array[counter])
            }
            counter += 1
        }
    }
    return numbers
}

// func for finding even or odd numbers using Repeat-While
func findNumbersUsingRepeat(array: [Int], type: Number) -> [Int] {
    var numbers: [Int] = []
    var counter = 0
    switch type {
    case .even:
        if array.count != 0 {
            repeat {
                if (array[counter] % 2 == 0) {
                    numbers.append(array[counter])
                }
                counter += 1
            } while counter < array.count
        }
    case .odd:
        if array.count != 0 {
            repeat {
                if (array[counter] % 2 != 0) {
                    numbers.append(array[counter])
                }
                counter += 1
            } while counter < array.count
        }
    }
    return numbers
}

// printing input array using three different ways
printArrayUsingFor(array: array)
printArrayUsingWhile(array: array)
printArrayUsingRepeat(array: array)


// output EVEN and ODD numbers using For-In, While, Repeat-While

print("\n====FILTERING====\n")

print("EVEN-numbers:")
printArrayUsingFor(array: findNumbersUsingFor(array: array, type: .even))
printArrayUsingWhile(array: findNumbersUsingWhile(array: array, type: .even))
printArrayUsingRepeat(array: findNumbersUsingRepeat(array: array, type: .even))

print("ODD-numbers:")
printArrayUsingFor(array: findNumbersUsingFor(array: array, type: .odd))
printArrayUsingWhile(array: findNumbersUsingWhile(array: array, type: .odd))
printArrayUsingRepeat(array: findNumbersUsingRepeat(array: array, type: .odd))

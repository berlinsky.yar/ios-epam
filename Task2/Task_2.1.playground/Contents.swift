import UIKit

let allStudents = [1,2,3,4,5,6,7,8,9,10]

enum Days {
    case Monday
    case Tuesday
    case Wednesday
}

// an attendance of single student
struct Attendance {
    var presentOnMonday: Bool
    var presentOnTuesday: Bool
    var presentOnWednesday: Bool
}

struct StudentAttendance {
    let studentID: Int
    var attendance: Attendance
}


// class with variety of filters used for exercise
class FilteringSelections {
    private let presentOnMonday = [1,2,5,6,7]
    private let presentOnTuesday = [3,6,8,10]
    private let presentOnWednesday = [1,3,7,9,10]
    private(set) var targetSelection: [StudentAttendance]  // input set of students
    
    // initializer takes a raw set of students' IDs
    init(allStudents: [Int]) {
        self.targetSelection = []
        for i in allStudents {
            // for each student we get attendance for each given day
            let attendance = Attendance(presentOnMonday: presentOnMonday.contains(i),
                                        presentOnTuesday: presentOnTuesday.contains(i),
                                        presentOnWednesday: presentOnWednesday.contains(i))
            self.targetSelection.append(StudentAttendance(studentID: i, attendance: attendance))
        }
    }
    
    /* method for getting set of students who were abcent/attending the University during 3 days
    -> depends on presence-parameter
    */
    func presentThreeDays(presence: Bool = true) -> [StudentAttendance] {
        var selection: [StudentAttendance] = []
        let presenceMark = (presence) ? 3 : 0
        for i in targetSelection {
            if numberOfPresentDays(for: i) == presenceMark {
                selection.append(i)
            }
        }
        return selection
    }
    
    
    /* method for getting set of students who were present during 2 days
    (or for 2 days except one ajusted day, if except-parameter isn't nil)
     */
    func presentOnlyTwoDays(targetSelection: [StudentAttendance], except: Days? = nil) -> [StudentAttendance] {
        var selection: [StudentAttendance] = []
        guard except != nil else {
            for i in targetSelection {
                if numberOfPresentDays(for: i) == 2 {
                    selection.append(i)
                }
            }
            return selection
        }
        selection = presentExeptDay(day: except!)
        selection = presentOnlyTwoDays(targetSelection: selection)
        return selection
    }
    
    // method for getting a number of days when student attended the University
    private func numberOfPresentDays(for student: StudentAttendance) -> Int {
            var counter = 0
            counter += student.attendance.presentOnMonday ? 1 : 0
            counter += student.attendance.presentOnTuesday ? 1 : 0
            counter += student.attendance.presentOnWednesday ? 1 : 0
            return counter
        }
    
    // method for getting set of students who weren't present on a certain day
    func presentExeptDay(day: Days) -> [StudentAttendance] {
        var selection: [StudentAttendance] = []
        switch day {
        case .Monday:
            for i in targetSelection {
                if !i.attendance.presentOnMonday {
                    selection.append(i)
                }
            }
        case .Tuesday:
            for i in targetSelection {
                if !i.attendance.presentOnTuesday {
                    selection.append(i)
                }
            }
        case .Wednesday:
            for i in targetSelection {
                if !i.attendance.presentOnWednesday {
                    selection.append(i)
                }
            }
        }
        return selection
    }
    
    func printStudentsAttendance(title: String, selection: [StudentAttendance]) {
        print("=====\(title)=====")
        if selection.count == 0 {
            print("\nEMPTY SET\n")
        } else {
        print("[ ", terminator: "")
            for i in 0..<selection.count {
            let printTerminator = i == selection.count - 1 ? " ]\n\n" : ", "
                print("\(selection[i].studentID)", terminator: printTerminator)
        }
        }
    }
}

let filter = FilteringSelections(allStudents: allStudents)
filter.printStudentsAttendance(title: "Students who attend University during 3 days", selection: filter.presentThreeDays())
filter.printStudentsAttendance(title: "Students who attend University during 2 days", selection: filter.presentOnlyTwoDays(targetSelection: filter.targetSelection))
filter.printStudentsAttendance(title: "Students who attend University during 2 days except Tuesday", selection: filter.presentOnlyTwoDays(targetSelection: filter.targetSelection, except: .Tuesday))
filter.printStudentsAttendance(title: "Students who missed all classes", selection: filter.presentThreeDays(presence: false))
